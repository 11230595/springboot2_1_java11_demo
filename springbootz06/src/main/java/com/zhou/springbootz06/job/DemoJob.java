package com.zhou.springbootz06.job;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 定时任务演示类
 * @author zhoudong
 */
@Component
public class DemoJob {
    private Logger log = LoggerFactory.getLogger(getClass());

    // 自本次开始，每10秒执行一次
    @Scheduled(fixedRate = 10000)
    public void fixedRate() {
        log.info(" ** fixedRate,运行..{} ",new Date());
    }

    // 自本次结束，每10秒执行一次
    @Scheduled(fixedDelay = 10000)
    public void fixedDelay() {
        log.info(" ** fixedDelay,运行..{} ",new Date());
    }

    // 第一次延迟1秒执行，当执行完后每2秒再执行
    @Scheduled(initialDelay = 1000, fixedDelay = 2000)
    public void delay() {
        log.info(" ** delay,运行..{}", new Date());
    }

    //每天10点54分00秒时执行
    @Scheduled(cron = "00 38 20 * * ?")
    public void cron() {
        log.info(" ** cron,运行..{}", new Date());
    }
}
