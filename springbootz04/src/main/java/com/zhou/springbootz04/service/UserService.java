package com.zhou.springbootz04.service;

import com.zhou.springbootz04.entity.User;

public interface UserService {
    int insert(User record);
    User selectByPrimaryKey(Integer id);
}
