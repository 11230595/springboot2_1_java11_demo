package com.zhou.springbootz04.controller;

import com.zhou.springbootz04.entity.User;
import com.zhou.springbootz04.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

@Controller
public class UserController {
    @Resource
    private UserService userService;
    // 首页
    @GetMapping("")
    public String index(){
        return "user";
    }
    // 保存
    @PostMapping("/save")
    public String save(User user){
        return "redirect:/user_detail/" + userService.insert(user);
    }
    // 获取用户信息
    @GetMapping("/user_detail/{id}")
    public ModelAndView detail(@PathVariable int id){
        var modelAndView = new ModelAndView("user_detail");
        modelAndView.addObject("user",userService.selectByPrimaryKey(id));
        return modelAndView;
    }
}
