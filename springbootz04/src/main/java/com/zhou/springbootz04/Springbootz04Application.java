package com.zhou.springbootz04;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.zhou.springbootz04.mapper")
public class Springbootz04Application {

	public static void main(String[] args) {
		SpringApplication.run(Springbootz04Application.class, args);
	}

}

