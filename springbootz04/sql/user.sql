/*
 Navicat Premium Data Transfer

 Source Server         : 172.28.1.24
 Source Server Type    : MySQL
 Source Server Version : 50626
 Source Host           : 172.28.1.24
 Source Database       : springbootz

 Target Server Type    : MySQL
 Target Server Version : 50626
 File Encoding         : utf-8

 Date: 12/23/2018 22:21:07 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `USER_NAME` varchar(64) NOT NULL COMMENT '用户名',
  `USER_CODE` varchar(64) NOT NULL COMMENT '用户Code',
  `USER_DESC` varchar(255) DEFAULT NULL COMMENT '用户描述',
  `CREATE_TIME` datetime NOT NULL COMMENT '创建时间',
  `UPDATE_TIME` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
