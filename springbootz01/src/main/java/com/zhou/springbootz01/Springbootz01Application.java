package com.zhou.springbootz01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootz01Application {

	public static void main(String[] args) {
		SpringApplication.run(Springbootz01Application.class, args);
	}

}

