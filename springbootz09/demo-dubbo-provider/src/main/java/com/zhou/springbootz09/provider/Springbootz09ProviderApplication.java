package com.zhou.springbootz09.provider;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableDubbo
@SpringBootApplication
public class Springbootz09ProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springbootz09ProviderApplication.class, args);
    }

}

