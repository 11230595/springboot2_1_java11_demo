package com.zhou.springbootz09.dubboprovider.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zhou.springbootz09.api.dubboprovider.DemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 服务提供者
 * @author zhoudong
 */
@Service
public class DemoServiceImpl implements DemoService {
    private Logger log = LoggerFactory.getLogger(getClass()); // 日志
    @Override
    public String sayHello(String name) {
        log.info("你好：{}，我是服务提供者，这是第一个Dubbo分布式应用",name);
        return String.format("你好：%s，我是服务提供者，这是第一个Dubbo分布式应用",name);
    }
}
