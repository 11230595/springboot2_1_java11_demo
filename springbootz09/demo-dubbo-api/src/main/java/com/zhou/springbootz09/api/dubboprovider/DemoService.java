package com.zhou.springbootz09.api.dubboprovider;

/**
 * Dubbo service
 * @author zhoudong
 */
public interface DemoService {
    String sayHello(String name);
}
