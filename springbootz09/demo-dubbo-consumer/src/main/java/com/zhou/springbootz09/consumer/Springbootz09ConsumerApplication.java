package com.zhou.springbootz09.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootz09ConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springbootz09ConsumerApplication.class, args);
    }

}

