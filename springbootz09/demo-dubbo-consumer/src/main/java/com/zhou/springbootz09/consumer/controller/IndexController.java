package com.zhou.springbootz09.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zhou.springbootz09.api.dubboprovider.DemoService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Dubbo 演示
 */
@RestController
public class IndexController {
    private Logger log = LoggerFactory.getLogger(getClass()); // 日志
    @Reference
    private DemoService demoService;

    @ApiOperation(value = "Dubbo演示", notes = "Dubbo演示", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "name", required = true)
    })
    @GetMapping("/index")
    public Map<String,String> index(@RequestParam String name){
        log.info("服务消费者收到请求参数：{}，开始请求远程接口...",name);
        return Map.of("result",demoService.sayHello(name));
    }
}
