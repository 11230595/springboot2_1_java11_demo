package com.zhou.springbootz08;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootz08Application {

    public static void main(String[] args) {
        SpringApplication.run(Springbootz08Application.class, args);
    }

}

