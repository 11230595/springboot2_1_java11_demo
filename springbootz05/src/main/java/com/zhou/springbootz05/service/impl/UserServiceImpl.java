package com.zhou.springbootz05.service.impl;


import com.zhou.springbootz05.entity.User;
import com.zhou.springbootz05.mapper.UserMapper;
import com.zhou.springbootz05.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public int insert(User record) {
        record.setCreateTime(new Date());
        record.setUpdateTime(new Date());
        userMapper.insert(record);
        return record.getId();
    }

    @Override
    public User selectByPrimaryKey(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }
}
