package com.zhou.springbootz05.service;


import com.zhou.springbootz05.entity.User;

public interface UserService {
    int insert(User record);
    User selectByPrimaryKey(Integer id);
}
