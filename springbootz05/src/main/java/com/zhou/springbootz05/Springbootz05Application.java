package com.zhou.springbootz05;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.zhou.springbootz05.mapper")
public class Springbootz05Application {

    public static void main(String[] args) {
        SpringApplication.run(Springbootz05Application.class, args);
    }

}

