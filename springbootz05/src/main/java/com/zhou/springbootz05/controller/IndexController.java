package com.zhou.springbootz05.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class IndexController {
    private Logger log = LoggerFactory.getLogger(getClass()); // 日志

    @ApiOperation(value = "获取用户信息接口", notes = "获取用户信息接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "用户名", required = true),
            @ApiImplicitParam(name = "userCode", value = "用户Code", required = true)
    })
    @RequestMapping(value = "/load_user_info",method = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
    public Map loadUserInfo(@RequestParam String userName, @RequestParam String userCode){
        log.info("【load_user_info】获取到参数：userName:{}，userCode:{}",userName,userCode);
        return Map.of("userCode",userCode,"userName",userName,"time", System.currentTimeMillis());
    }

    @ApiOperation(value = "获取用户", notes = "获取用户", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户ID", required = true),
            @ApiImplicitParam(name = "userCode", value = "用户Code")
    })
    @GetMapping("/load_user")
    public Map loadUser(@RequestParam String userId, @RequestParam(required = false) String userCode){
        log.info("【load_user】获取到参数：userName:{}，userCode:{}",userId,userCode);
        if(StringUtils.isEmpty(userCode)) userCode = "";
        return Map.of("userCode",userCode,"userId",userId,"time", System.currentTimeMillis());
    }
}
