package com.zhou.springbootz05.controller;

import com.zhou.springbootz05.entity.User;
import com.zhou.springbootz05.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

@RestController
public class UserController {
    private Logger log = LoggerFactory.getLogger(getClass()); // 日志
    @Resource
    private UserService userService;

    // 获取用户信息
    @ApiOperation(value = "获取用户信息接口", notes = "获取用户信息接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户id", required = true)
    })
    @GetMapping("/user_detail")
    public User detail(@RequestParam int id){
        log.info("【从数据库获取用户信息接口收到信息】id:{}",id);
        return userService.selectByPrimaryKey(id);
    }
}
