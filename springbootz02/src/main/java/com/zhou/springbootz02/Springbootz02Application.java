package com.zhou.springbootz02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootz02Application {

    public static void main(String[] args) {
        SpringApplication.run(Springbootz02Application.class, args);
    }

}

