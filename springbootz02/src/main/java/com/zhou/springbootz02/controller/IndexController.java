package com.zhou.springbootz02.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {
    private Logger log = LoggerFactory.getLogger(getClass()); // 日志

    @GetMapping("/index")
    public String index(){
        log.info("访问首页");
        return "index";
    }

    /**
     * 带参数访问首页
     * @param modelAndView
     * @param name
     * @return
     */
    @GetMapping("/index/{name}")
    public String index(ModelAndView modelAndView
            , @PathVariable String name){
        log.info("{}：访问首页",name);
        modelAndView.addObject("name",name);
        return "index";
    }
}
