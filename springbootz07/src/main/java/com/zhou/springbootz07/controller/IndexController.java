package com.zhou.springbootz07.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Springboot2.1 https http2
 * @author zhoudong
 */
@RestController
public class IndexController {
    private Logger log = LoggerFactory.getLogger(getClass()); // 日志

    @GetMapping("/index")
    public Map index(){
        log.info("访问index....");
        return Map.of("msg","hello.");
    }
}
