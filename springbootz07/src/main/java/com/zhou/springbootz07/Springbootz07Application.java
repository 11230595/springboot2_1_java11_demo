package com.zhou.springbootz07;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootz07Application {

    public static void main(String[] args) {
        SpringApplication.run(Springbootz07Application.class, args);
    }

}

