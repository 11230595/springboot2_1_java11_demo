package com.zhou.springbootz10.servletweb;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
public class ServletWebController {

    @GetMapping("servletweb")
    public Map<String,String> servletweb(){
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Map.of("msg","servletweb");
    }

}
