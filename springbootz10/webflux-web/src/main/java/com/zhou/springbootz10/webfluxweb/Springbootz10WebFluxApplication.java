package com.zhou.springbootz10.webfluxweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootz10WebFluxApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springbootz10WebFluxApplication.class, args);
    }

}

