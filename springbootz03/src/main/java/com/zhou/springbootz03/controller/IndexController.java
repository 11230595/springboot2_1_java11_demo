package com.zhou.springbootz03.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

@Controller
public class IndexController {

    @GetMapping("/index")
    public String index(){
        try {
            TimeUnit.SECONDS.sleep(1); // 延迟1秒
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "index";
    }

    // 带参数访问首页
    @GetMapping("/index/{name}")
    public String index(ModelAndView modelAndView
            , @PathVariable String name){
        modelAndView.addObject("name",name);
        return "index";
    }
    // webflux
    @GetMapping("/webfulx_index")
    public Mono<String> webfulxIndex(){
        return Mono.just("index").delayElement(Duration.ofSeconds(1)); // 延迟1秒
    }

}
