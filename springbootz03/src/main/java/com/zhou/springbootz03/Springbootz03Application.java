package com.zhou.springbootz03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootz03Application {

    public static void main(String[] args) {
        SpringApplication.run(Springbootz03Application.class, args);
    }

}

